const cheerio = require('cheerio');
const jsonframe = require('jsonframe-cheerio');
const rp = require('request-promise');

const Diary = require("./diaryFactory.js");
const weekScraper = require("./weekScraper.js");

const diaryFrame = {
  "title": "h1",
  "urls": [ ".day_items a.day_item @ href" ],
  "weekNumbersWithUrl": [ ".day_items a.day_item .num" ],
  "weekNumbersWithoutUrl": [ ".day_items span.day_item .num" ],
}

function diaryScraper(url) {
  return rp(url)
    .then(function(html){
      let $ = cheerio.load(html)

      jsonframe($) // initializing the plugin

      const diaryDescription = $('body').scrape(diaryFrame);

      const numberOfWeeks = diaryDescription.weekNumbersWithUrl.length + diaryDescription.weekNumbersWithoutUrl.length;
      const diary = new Diary(diaryDescription.title, numberOfWeeks);
      const promises = Array();
      for(const url of diaryDescription.urls){
        promises.push(weekScraper("https://growdiaries.com" + url)
          .then((week) => {
            diary.addWeek(week);
          }))
      }

      return Promise.all(promises).then(() => {return diary;})
    })
    .catch(function(err){
      throw err;
    });
}

module.exports = diaryScraper;
