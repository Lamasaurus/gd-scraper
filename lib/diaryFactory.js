module.exports = class{
  constructor(title, numberOfWeeks){
    this.title = title;
    this.numberOfWeeks = numberOfWeeks;
    this.growWeeks = Array();
    this.harvestWeeks = Array();
  }

  addWeek(week){
    const weeks = week.isGrowWeek ? this.growWeeks : this.harvestWeeks;
    weeks.push(week);

    // Harvest weeks do not count as grow week
    if(!week.isGrowWeek)
      this.numberOfWeeks--;

    // sort the weeks
    weeks.sort((week1, week2) => {
      return week1.getWeekNumber() > week2.getWeekNumber();
    });
  }

  getGrowWeek(number){
    for(const week of this.growWeeks)
      if(week.getWeekNumber() == number)
        return week;
  }

  getHarvestWeeks(){
    return this.harvestWeeks;   
  }

  getWeekAtribute(number, attribute, preferedUnits){
    const week = this.getGrowWeek(number);
    return week ? week.getParameterValue(attribute, preferedUnits) : null;
  }

  getGrowAttributeArray(attribute, preferedUnits){
    const values = Array();
    for(let i = 1; i <= this.numberOfWeeks; i++)
      values.push(this.getWeekAtribute(i, attribute, preferedUnits));
    return values;
  }

  getFertArrays(preferedUnits){
    const values = Object();
    for(let i = 1; i <= this.numberOfWeeks; i++){
      const week = this.getGrowWeek(i);
      if(week){
        const fertValues = week.getFertValues(preferedUnits);
        for(const fertValue of fertValues){

          // If this fert is not yet in the values, add it
          if(!(fertValue.label in values))
            values[fertValue.label] = Array();

          // Add null values if the fert has not been used in earlier weeks
          while(values[fertValue.label].length < i - 1)
            values[fertValue.label].push(null);

          // Push the next value
          values[fertValue.label].push(fertValue);
        }
      }
    }

    return values;
  }
}
