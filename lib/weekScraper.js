const cheerio = require('cheerio');
const jsonframe = require('jsonframe-cheerio');
const rp = require('request-promise');

const weekFactory = require('./weekFactory.js');

const weekFrame = {
  "parameters": {
    _s: ".parameters .parameters_item .statistic_box",
    _d: [{
      "value": ".value",
      "unit": ".value .measure_toggle .active",
      "label": ".label:last-of-type",
    }]
  },

  "fert": {
    _s: ".fert_item",
    _d: [{
      "name": ".fert_name",
      "value": [".fert_sel_vl"],
    }]
  },

  "strain": {
    _s: ".seed_box",
    _d: {
      "name": ".seed_box_brand .header",
      "breeder": ".seed_box_brand .meta .stay",
      "rating": ".seed_box_brand .rating @ data-rating",
      "growEase": ".seed_box_grow p:last-of-type",
      "resistance": ".seed_box_resi p:last-of-type",
    }
  },

  "textPost": ".text_post",
}

module.exports = function (url) {
  return rp(url)
    .then(function(html){
      let $ = cheerio.load(html)

      jsonframe($) // initializing the plugin

      const week = $('body').scrape(weekFrame);

      return weekFactory(week); 
    })
    .catch(function(err){
      throw err;
    });
}
