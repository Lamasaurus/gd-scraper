const paramMap = {
  "Flowering": "weekNumber",
  "Vegetation": "weekNumber",
  "HARVEST": "weekNumber",
  "Height": "height",
  "Light Schedule": "lightScheduel",
  "Day Air Temperature": "dayAirTemp",
  "Night Air Temperature": "nightAirTemp",
  "pH": "ph",
  "TDS": "tds",
  "EC": "ec",
  "Air Humidity": "airHumidity",
  "Smell": "smell",
  "Solution Temperature": "solutionTemp",
  "Substrate Temperature": "substrateTemp",
  "Pot Size": "potSize",
  "Lamp To Plant Distance": "lampToPlantDistance",
  "Watering Volume Per Plant Per 24h": "water",
  "CO₂ Level": "co2",
  "Number of plants harvested": "plantCount",
  "Grow Room size": "roomSize",
  "Light": "lightWatt",
  "Bud dry weight": "dryWeight",
  "Bud wet weight": "wetWeight",
  "Total light power used": "totalWatt",
  "Strain Name": "strainName",
  "Breeder": "breeder",
  "Strain Rating": "rating",
  "Ease of grow": "growEase",
  "Resistance": "resistance",
  "Description": "description",
}

const preferedUnits = [
  "cm",
  "l",
  "°C",
  "PPM",
  "ml/l",
  "m²",
  "watt",
  "g",
  "%",
  "plants",
  "weeks",
  "mS/cm",
  "pH",
  "hrs",
]

const smellMap = {
  "No Smell": 0,
  "Weak": 33,
  "Normal": 66,
  "Strong": 100,
}

class Parameter{
  constructor(label){
    this.label = label;
    this.values = Object();
  }

  getValueFromPreferedUnitList(preferedUnits){
    for(const valueUnit in this.values){
      if(preferedUnits.includes(valueUnit))
        return {value: this.values[valueUnit], unit: valueUnit, label: this.label};
    }
    return {value: null, unit: null, label: this.label};
  }

  getValue(unit){
    return unit in this.values ? this.values[unit] : null;
  }

  setValue(value, unit){
    this.values[unit] = value;
  }
}

class GrowWeek{

  constructor(week) {
    for(const param of week.parameters){
      const paramName = paramMap[param.label];
      const activeParameter = this.getParameter(paramName, param.label);
      let unit = param.unit;

      // If there is no unit
      if(!unit) {
        switch(paramName){
          case "weekNumber":
            unit = "weeks";
            break;
          case "airHumidity":
          case "smell":
            unit = "%";
            break;
          case "ph":
            unit = "pH";
            break;
          default:
            unit = param.value.split(" ")[1];
        }
      }

      // Parse the value to a number
      let parameterValue = parseFloat(param.value);
      // The number could also come after the unit
      if(isNaN(parameterValue))
        parameterValue = parseFloat(param.value.split(" ")[1]);
      // Values can also not be numeric
      if(isNaN(parameterValue))
        parameterValue = param.value;

      // Smell is special
      if(paramName === "smell")
        parameterValue = smellMap[param.value];

      activeParameter.setValue(parameterValue, unit);
    }

    this.textPost = week.textPost;

    this.fert = Object();
    for(const fert of week.fert){
      const newFert = new Parameter(fert.name, fert.name);

      for(const value of fert.value)
        newFert.setValue(parseFloat(value), value.split(" ")[1]);

      this.fert[fert.name] = newFert;
    }
  }

  // Get the parameter with a certain name or a new one
  getParameter(parameterName, parameterLabel){
    if(!(parameterName in this))
      this[parameterName] = new Parameter(parameterLabel);
    return this[parameterName];
  }

  getWeekNumber() {
    return this.getParameterValue("weekNumber").value;
  }

  get isGrowWeek(){
    return true;
  }

  getParameterValue(parameterName, userPreferedUnits){
    const activeParameter = this.getParameter(parameterName);
    const pu = userPreferedUnits ? userPreferedUnits : preferedUnits;
    return activeParameter.getValueFromPreferedUnitList(pu);
  }

  getFertValues(userPreferedUnits){
    const pu = userPreferedUnits ? userPreferedUnits : preferedUnits;
    const fertValues = Array();
    for(const fertName in this.fert)
      fertValues.push(this.fert[fertName].getValueFromPreferedUnitList(pu));
    return fertValues;
  }
}

class HarvestWeek extends GrowWeek{
  constructor(week) {
    super(week);

    this.addParameter("strainName", week.strain.name);
    this.addParameter("breeder", week.strain.breeder);
    this.addParameter("rating", week.strain.rating);
    this.addParameter("growEase", week.strain.growEase);
    this.addParameter("resistance", week.strain.resistance);
  }

  addParameter(name, value){
    this[name] = new Parameter(name);
    this[name].setValue(value, "");
  }

  get isGrowWeek(){
    return false;
  }

}

module.exports = function (week) {
  let isGrowWeek = false;
  for(const param of week.parameters) {
    if(param.value.startsWith("Week") || param.label == "HARVEST"){
      isGrowWeek = param.label !== "HARVEST";
      break;
    }
  }

  if(isGrowWeek)
    return new GrowWeek(week);
  else
    return new HarvestWeek(week);
}
